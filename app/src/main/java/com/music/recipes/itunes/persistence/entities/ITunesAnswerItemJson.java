package com.music.recipes.itunes.persistence.entities;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by lectricas on 30.06.2017.
 */

@Getter
@Setter
public class ITunesAnswerItemJson {

    @SerializedName("kind")
    private String kind;

    @SerializedName("artistName")
    private String artistName;

    @SerializedName("collectionName")
    private String collectionName;

    @SerializedName("trackName")
    private String trackName;

    @SerializedName("artworkUrl60")
    private String artworkUrl60;

    @SerializedName("trackTimeMillis")
    private int trackTimeMillis;

    @SerializedName("primaryGenreName")
    private String primaryGenreName;
}
