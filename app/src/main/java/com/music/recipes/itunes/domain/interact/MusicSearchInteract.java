package com.music.recipes.itunes.domain.interact;


import com.music.recipes.itunes.domain.model.ITunesAnswerModel;

import io.reactivex.Single;

/**
 * Created by lectricas on 30.06.2017.
 */

public interface MusicSearchInteract {
    Single<ITunesAnswerModel> getMusicList(String searchQuery);
}
