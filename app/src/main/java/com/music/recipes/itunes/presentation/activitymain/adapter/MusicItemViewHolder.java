package com.music.recipes.itunes.presentation.activitymain.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.frogermcs.recipes.itunes.R;
import com.music.recipes.itunes.domain.model.ITunesItemModel;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lectricas on 02-Jul-17.
 */

public class MusicItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.song_image)
    ImageView songImage;
    @BindView(R.id.song_name)
    TextView songName;

    Context context;

    public MusicItemViewHolder(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void bind(ITunesItemModel iTunesItemModel) {
        Glide.with(context)
                .load(iTunesItemModel.getArtworkUrl60())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(songImage);

        songName.setText(iTunesItemModel.getTrackName());
    }
}
