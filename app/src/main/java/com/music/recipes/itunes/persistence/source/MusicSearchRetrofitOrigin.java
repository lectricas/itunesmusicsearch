package com.music.recipes.itunes.persistence.source;

import com.music.recipes.itunes.domain.model.ITunesAnswerModel;
import com.music.recipes.itunes.persistence.source.mapper.MusicAnswerMapper;
import com.music.recipes.itunes.presentation.Utils;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by lectricas on 30.06.2017.
 */

public class MusicSearchRetrofitOrigin implements MusicSearchOrigin {

    ITunesRetrofitApi api;

    @Inject
    public MusicSearchRetrofitOrigin(ITunesRetrofitApi api) {
        this.api = api;
    }

    @Override
    public Single<ITunesAnswerModel> getMusic(String searchQuery) {
        return api.getSongList(searchQuery, Utils.ENTITY_SONG)
                .map(MusicAnswerMapper::answerJsonToModel);
    }
}
