package com.music.recipes.itunes.presentation;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.music.recipes.itunes.presentation.di.AppComponent;
import com.music.recipes.itunes.presentation.di.DaggerAppComponent;
import com.music.recipes.itunes.presentation.di.activity.ActivityComponentBuilder;
import com.music.recipes.itunes.presentation.di.activity.HasActivitySubcomponentBuilders;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;


public class MyApplication extends Application implements HasActivitySubcomponentBuilders {

    @Inject
    public Map<Class<? extends Activity>, Provider<ActivityComponentBuilder>> activityComponentBuilders;

    private AppComponent appComponent;

    public static HasActivitySubcomponentBuilders get(Context context) {
        return ((HasActivitySubcomponentBuilders) context.getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.create();
        appComponent.inject(this);
    }

    @Override
    public ActivityComponentBuilder getActivityComponentBuilder(Class<? extends Activity> activityClass) {
        return activityComponentBuilders.get(activityClass).get();
    }
}