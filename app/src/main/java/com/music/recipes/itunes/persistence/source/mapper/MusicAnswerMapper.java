package com.music.recipes.itunes.persistence.source.mapper;

import android.support.annotation.NonNull;
import android.text.TextUtils;


import com.music.recipes.itunes.domain.model.ITunesAnswerModel;
import com.music.recipes.itunes.domain.model.ITunesItemModel;
import com.music.recipes.itunes.persistence.entities.ITunesAnswerItemJson;
import com.music.recipes.itunes.persistence.entities.ITunesAnswerJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lectricas on 01-Jul-17.
 */

public class MusicAnswerMapper {

    @NonNull
    public static ITunesAnswerModel answerJsonToModel(ITunesAnswerJson entity) {
        List<ITunesItemModel> modelList = new ArrayList<>();

        if (entity.getResults() != null) {
            for (ITunesAnswerItemJson jn : entity.getResults()) {
                ITunesItemModel model = new ITunesItemModel(
                        TextUtils.isEmpty(jn.getKind()) ? "" : jn.getKind(),
                        TextUtils.isEmpty(jn.getArtistName()) ? "" : jn.getArtistName(),
                        TextUtils.isEmpty(jn.getTrackName()) ? "" : jn.getTrackName(),
                        TextUtils.isEmpty(jn.getArtworkUrl60()) ? "" : jn.getArtworkUrl60(),
                        jn.getTrackTimeMillis(),
                        TextUtils.isEmpty(jn.getPrimaryGenreName()) ? "" : jn.getPrimaryGenreName(),
                        TextUtils.isEmpty(jn.getCollectionName()) ? "" : jn.getCollectionName());
                modelList.add(model);
            }
        }
        return new ITunesAnswerModel(modelList, entity.getResultCount());
    }
}
