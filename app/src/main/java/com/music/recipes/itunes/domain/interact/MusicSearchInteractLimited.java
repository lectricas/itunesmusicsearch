package com.music.recipes.itunes.domain.interact;


import com.music.recipes.itunes.domain.model.ITunesAnswerModel;
import com.music.recipes.itunes.persistence.source.MusicSearchOrigin;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by lectricas on 30.06.2017.
 */

public class MusicSearchInteractLimited implements MusicSearchInteract {

    MusicSearchOrigin origin;

    @Inject
    public MusicSearchInteractLimited(MusicSearchOrigin origin) {
        this.origin = origin;
    }

    @Override
    public Single<ITunesAnswerModel> getMusicList(String searchQuery) {
        return origin.getMusic(searchQuery);
    }
}
