package com.music.recipes.itunes.presentation.di;

import com.frogermcs.recipes.itunes.BuildConfig;
import com.music.recipes.itunes.persistence.source.ITunesRetrofitApi;
import com.music.recipes.itunes.presentation.Utils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class AppModule {

    public AppModule() {
    }

    @Provides
    @Singleton
    public Utils provideUtils() {
        return new Utils();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        final OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(logging);
        }
        return clientBuilder.build();
    }

    @Provides
    @Singleton
    ITunesRetrofitApi provideRetrofitApi(OkHttpClient okHttpClient){
        Retrofit r = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return r.create(ITunesRetrofitApi.class);
    }

}