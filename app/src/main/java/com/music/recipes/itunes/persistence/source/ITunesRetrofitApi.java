package com.music.recipes.itunes.persistence.source;


import com.music.recipes.itunes.persistence.entities.ITunesAnswerJson;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by lectricas on 01-Jul-17.
 */

public interface ITunesRetrofitApi {

    @GET("search")
    Single<ITunesAnswerJson> getSongList(@Query("term")String artist,
                                         @Query("entity")String entity);
}
