package com.music.recipes.itunes.domain.model;

import android.support.annotation.NonNull;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by lectricas on 30.06.2017.
 */

@Getter
@Setter
public class ITunesAnswerModel {

    private int resultCount;

    @NonNull
    private List<ITunesItemModel> results;

    public ITunesAnswerModel(@NonNull List<ITunesItemModel> results, int resultCount) {
        this.resultCount = resultCount;
        this.results = results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ITunesAnswerModel that = (ITunesAnswerModel) o;

        if (resultCount != that.resultCount) return false;
        return results.equals(that.results);

    }

    @Override
    public int hashCode() {
        int result = resultCount;
        result = 31 * result + results.hashCode();
        return result;
    }
}
