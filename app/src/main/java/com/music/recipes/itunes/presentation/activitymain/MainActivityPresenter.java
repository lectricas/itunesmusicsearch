package com.music.recipes.itunes.presentation.activitymain;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.jakewharton.rxbinding2.InitialValueObservable;
import com.music.recipes.itunes.domain.interact.MusicSearchInteract;
import com.music.recipes.itunes.presentation.BasePresenter;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class MainActivityPresenter extends BasePresenter<MainActivityView> {

    private final MusicSearchInteract interactSearch;

    @Inject
    public MainActivityPresenter(MusicSearchInteract interact) {
        this.interactSearch = interact;
    }

    public void initMusicSearch(InitialValueObservable<CharSequence> searchString) {
        Disposable s1 =  searchString.debounce(600, TimeUnit.MILLISECONDS)
                .filter(charSequence -> charSequence.length() > 0)
                .flatMapSingle(charSequence -> interactSearch.getMusicList(charSequence.toString()))
                // TODO: 03-Jul-17 https://stackoverflow.com/questions/35384247/rxtextview-text-changes-retrofit-call-leads-to-interruptedioexception
                .onErrorResumeNext(Observable.empty())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(answerModel -> {
                    getViewState().onMusicQueried(answerModel.getResults());
                }, throwable -> {
                    getViewState().errorOccurred(throwable);
                });
        unsubscribeOnDestroy(s1);
    }
}
