package com.music.recipes.itunes;


import com.frogermcs.recipes.itunes.BuildConfig;
import com.music.recipes.itunes.domain.model.ITunesAnswerModel;
import com.music.recipes.itunes.domain.model.ITunesItemModel;
import com.music.recipes.itunes.persistence.entities.ITunesAnswerItemJson;
import com.music.recipes.itunes.persistence.entities.ITunesAnswerJson;
import com.music.recipes.itunes.persistence.source.ITunesRetrofitApi;
import com.music.recipes.itunes.persistence.source.MusicSearchRetrofitOrigin;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class PersistenceMapperRetrofitTest {

    private final NetworkBehavior behavior = NetworkBehavior.create();
    private final TestObserver<ITunesAnswerModel> testObserver = TestObserver.create();
    private ITunesRetrofitApi mockService;

    private MusicSearchRetrofitOrigin fakeMusicSearchOrigin;

    @Before
    public void setUp() throws Exception {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL).build();

        MockRetrofit mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior).build();

        final BehaviorDelegate<ITunesRetrofitApi> delegate = mockRetrofit.create(ITunesRetrofitApi.class);

        mockService = new ITunesRetrofitApi() {
            @Override
            public Single<ITunesAnswerJson> getSongList(String searchQuery, String entity) {
                return delegate.returningResponse(generateJson()).getSongList(searchQuery, entity);
            }
        };

        fakeMusicSearchOrigin = new MusicSearchRetrofitOrigin(mockService);
    }

    @Test
    public void testSuccessResponse() throws Exception {
        givenNetworkFailurePercentIs(0);
        fakeMusicSearchOrigin.getMusic("ArtistName").subscribe(testObserver);
        testObserver.assertValue(generateModel());
        testObserver.assertComplete();
    }

    @Test
    public void testFailureResponse() throws Exception {
        givenNetworkFailurePercentIs(100);
        fakeMusicSearchOrigin.getMusic("ArtistName").subscribe(testObserver);
        testObserver.assertNoValues();
        testObserver.assertError(IOException.class);
    }

    private void givenNetworkFailurePercentIs(int failurePercent) {
        behavior.setDelay(0, MILLISECONDS);
        behavior.setVariancePercent(0);
        behavior.setFailurePercent(failurePercent);
    }

    private ITunesAnswerJson generateJson() {
        ITunesAnswerJson json = new ITunesAnswerJson();
        json.setResultCount(2);
        ITunesAnswerItemJson itemJson = new ITunesAnswerItemJson();
        itemJson.setKind("1");
        itemJson.setArtistName("2");
        itemJson.setTrackName("3");
        itemJson.setArtworkUrl60("4");
        itemJson.setTrackTimeMillis(500);
        itemJson.setPrimaryGenreName("6");
        itemJson.setCollectionName("7");
        List<ITunesAnswerItemJson> items = Collections.singletonList(itemJson);
        json.setResults(items);
        return json;
    }

    private ITunesAnswerModel generateModel() {
        return new ITunesAnswerModel(
                Collections.singletonList(new ITunesItemModel("1", "2", "3", "4", 500, "6", "7")),
                2);
    }
}