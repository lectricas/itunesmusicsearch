package com.music.recipes.itunes.persistence.source;



import com.music.recipes.itunes.domain.model.ITunesAnswerModel;

import io.reactivex.Single;

/**
 * Created by lectricas on 30.06.2017.
 */

public interface MusicSearchOrigin {
    Single<ITunesAnswerModel> getMusic(String searchQuery);
}
