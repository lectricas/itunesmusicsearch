package com.music.recipes.itunes.persistence.entities;



import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by lectricas on 30.06.2017.
 */

@Getter
@Setter
public class ITunesAnswerJson {
    @SerializedName("resultCount")
    private int resultCount;
    @SerializedName("results")
    private List<ITunesAnswerItemJson> results;
}
