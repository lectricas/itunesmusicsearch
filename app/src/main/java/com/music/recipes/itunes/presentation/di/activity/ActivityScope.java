package com.music.recipes.itunes.presentation.di.activity;

import javax.inject.Scope;

/**
 * Created by froger_mcs on 14/09/16.
 */

@Scope
public @interface ActivityScope {
}
