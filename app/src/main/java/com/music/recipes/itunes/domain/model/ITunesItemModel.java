package com.music.recipes.itunes.domain.model;

import android.support.annotation.NonNull;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by lectricas on 30.06.2017.
 */
@Getter
@Setter
public class ITunesItemModel {

    @NonNull
    private final String kind;

    @NonNull
    private final String artistName;

    @NonNull
    private final String collectionName;

    @NonNull
    private final String trackName;

    @NonNull
    private final String artworkUrl60;

    @NonNull
    private final String primaryGenreName;

    private final int trackTimeMillis;

    public ITunesItemModel(@NonNull String kind, @NonNull String artistName, @NonNull String trackName,
                           @NonNull String artworkUrl60, int trackTimeMillis,
                           @NonNull String primaryGenreName, @NonNull String collectionName) {
        this.kind = kind;
        this.artistName = artistName;
        this.trackName = trackName;
        this.artworkUrl60 = artworkUrl60;
        this.trackTimeMillis = trackTimeMillis;
        this.primaryGenreName = primaryGenreName;
        this.collectionName = collectionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ITunesItemModel that = (ITunesItemModel) o;

        if (trackTimeMillis != that.trackTimeMillis) return false;
        if (!kind.equals(that.kind)) return false;
        if (!artistName.equals(that.artistName)) return false;
        if (!collectionName.equals(that.collectionName)) return false;
        if (!trackName.equals(that.trackName)) return false;
        if (!artworkUrl60.equals(that.artworkUrl60)) return false;
        return primaryGenreName.equals(that.primaryGenreName);

    }

    @Override
    public int hashCode() {
        int result = kind.hashCode();
        result = 31 * result + artistName.hashCode();
        result = 31 * result + collectionName.hashCode();
        result = 31 * result + trackName.hashCode();
        result = 31 * result + artworkUrl60.hashCode();
        result = 31 * result + trackTimeMillis;
        result = 31 * result + primaryGenreName.hashCode();
        return result;
    }
}
