package com.music.recipes.itunes.presentation.di.activity;

import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;
import javax.inject.Provider;

public class PresenterProvider<Presenter extends MvpPresenter> {

    @Inject
    public Provider<Presenter> presenterProvider;

    public PresenterProvider() {
    }

    public Presenter getPresenter() {
        return presenterProvider.get();
    }
}