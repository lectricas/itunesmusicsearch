package com.music.recipes.itunes.presentation.di;

import com.music.recipes.itunes.domain.di.DomainModule;
import com.music.recipes.itunes.persistence.di.PersistenceModule;
import com.music.recipes.itunes.presentation.MyApplication;
import com.music.recipes.itunes.presentation.di.activity.ActivityBindingModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(
        modules = {
                AppModule.class,
                ActivityBindingModule.class,
                DomainModule.class,
                PersistenceModule.class
        }
)
public interface AppComponent {
    MyApplication inject(MyApplication application);
}