package com.music.recipes.itunes;

import android.app.Activity;


import com.music.recipes.itunes.presentation.MyApplication;
import com.music.recipes.itunes.presentation.di.activity.ActivityComponentBuilder;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Provider;


public class ApplicationMock extends MyApplication {

    public void putActivityComponentBuilder(final ActivityComponentBuilder builder, Class<? extends Activity> cls) {
        Map<Class<? extends Activity>, Provider<ActivityComponentBuilder>> activityComponentBuilders = new HashMap<>(this.activityComponentBuilders);
        activityComponentBuilders.put(cls, new Provider<ActivityComponentBuilder>() {
            @Override
            public ActivityComponentBuilder get() {
                return builder;
            }
        });
        this.activityComponentBuilders = activityComponentBuilders;
    }
}