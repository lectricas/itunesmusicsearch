package com.music.recipes.itunes.presentation;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpPresenter;
import com.music.recipes.itunes.presentation.activitymain.MainActivityComponent;
import com.music.recipes.itunes.presentation.di.activity.ActivityComponent;
import com.music.recipes.itunes.presentation.di.activity.HasActivitySubcomponentBuilders;
import com.music.recipes.itunes.presentation.di.activity.PresenterProvider;


public abstract class BaseActivity extends MvpAppCompatActivity {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupActivityComponent();
        super.onCreate(savedInstanceState);
    }

    private void setupActivityComponent() {
        injectMembers(MyApplication.get(this));
    }

    protected abstract void injectMembers(HasActivitySubcomponentBuilders hasActivitySubcomponentBuilders);

    public void setActivityComponent(MainActivityComponent activityComponent) {
        this.activityComponent = activityComponent;
    }

    protected MvpPresenter providePresenter() {
        PresenterProvider<MvpPresenter> presenterProvider = new PresenterProvider<>();
        activityComponent.inject(presenterProvider);
        return presenterProvider.getPresenter();
    }
}
