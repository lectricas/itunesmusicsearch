package com.music.recipes.itunes.persistence.di;


import com.music.recipes.itunes.persistence.source.MusicSearchOrigin;
import com.music.recipes.itunes.persistence.source.MusicSearchRetrofitOrigin;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * Created by lectricas on 30.06.2017.
 */

@Module
public abstract class PersistenceModule {
    @Binds
    @Singleton
    public abstract MusicSearchOrigin bindPollInteractor(MusicSearchRetrofitOrigin interactor);
}