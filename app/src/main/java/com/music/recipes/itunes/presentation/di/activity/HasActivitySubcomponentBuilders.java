package com.music.recipes.itunes.presentation.di.activity;

import android.app.Activity;


public interface HasActivitySubcomponentBuilders {
    ActivityComponentBuilder getActivityComponentBuilder(Class<? extends Activity> activityClass);
}