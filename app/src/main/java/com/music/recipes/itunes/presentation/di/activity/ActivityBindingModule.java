package com.music.recipes.itunes.presentation.di.activity;



import com.music.recipes.itunes.presentation.activitymain.MainActivity;
import com.music.recipes.itunes.presentation.activitymain.MainActivityComponent;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module(
        subcomponents = {
                MainActivityComponent.class,
        })
public abstract class ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    public abstract ActivityComponentBuilder mainActivityComponentBuilder(MainActivityComponent.Builder impl);
}