package com.music.recipes.itunes.domain.di;


import com.music.recipes.itunes.domain.interact.MusicSearchInteract;
import com.music.recipes.itunes.domain.interact.MusicSearchInteractLimited;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class DomainModule {
    @Binds
    @Singleton
    public abstract MusicSearchInteract bindMusicInteractor(MusicSearchInteractLimited interactor);
}
