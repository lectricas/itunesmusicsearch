package com.music.recipes.itunes;


import com.music.recipes.itunes.domain.interact.MusicSearchInteract;
import com.music.recipes.itunes.domain.interact.MusicSearchInteractLimited;
import com.music.recipes.itunes.domain.model.ITunesAnswerModel;
import com.music.recipes.itunes.domain.model.ITunesItemModel;
import com.music.recipes.itunes.persistence.source.MusicSearchOrigin;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import io.reactivex.Single;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class GetMusicDomainTest {

    MusicSearchOrigin fakeMusicSearchOrigin;

    MusicSearchInteract musicSearchInteract;

    ITunesAnswerModel fakeITunesAnswerModel;
    ITunesAnswerModel expectedITunesAnswerModel;

    Single<ITunesAnswerModel> fakeSingleMusicAnswer;
    Single<ITunesAnswerModel> expectedSingleMusicAnswer;

    @Before
    public void setUp() {
        fakeMusicSearchOrigin = mock(MusicSearchOrigin.class);

        fakeITunesAnswerModel = setUpITunesAnswer(Collections.singletonList(
                new ITunesItemModel("1", "2", "3", "1", 1, "6", "7")), 1);
        expectedITunesAnswerModel = setUpITunesAnswer(Collections.singletonList(
                new ITunesItemModel("1", "2", "3", "1", 1, "6", "7")), 1);

        fakeSingleMusicAnswer = Single.just(fakeITunesAnswerModel);
        expectedSingleMusicAnswer = Single.just(expectedITunesAnswerModel);
    }

    private ITunesAnswerModel setUpITunesAnswer(List<ITunesItemModel> answerModel, int resultCount) {
        ITunesAnswerModel answersListModel = new ITunesAnswerModel(answerModel, resultCount);
        answersListModel.setResults(answerModel);
        answersListModel.setResultCount(resultCount);
        return answersListModel;
    }

    @Test
    public void execute_noException() throws Exception {
        when(fakeMusicSearchOrigin.getMusic("Wham!")).thenReturn(fakeSingleMusicAnswer);
        musicSearchInteract = new MusicSearchInteractLimited(fakeMusicSearchOrigin);
        musicSearchInteract.getMusicList("Wham!").test().assertResult(expectedITunesAnswerModel);
    }

    @Test
    public void execute_onlyException() throws Exception {
        Throwable fakeException = new Throwable("Oops");

        Single<ITunesAnswerModel> fakeSingleWithException = Single.error(fakeException);
        when(fakeMusicSearchOrigin.getMusic("Wham!")).thenReturn(fakeSingleWithException);

        musicSearchInteract = new MusicSearchInteractLimited(fakeMusicSearchOrigin);
        musicSearchInteract.getMusicList("Wham!").test().assertError(fakeException);
    }
}