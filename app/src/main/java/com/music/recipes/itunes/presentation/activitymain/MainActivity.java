package com.music.recipes.itunes.presentation.activitymain;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.frogermcs.recipes.itunes.R;


import com.jakewharton.rxbinding2.widget.RxTextView;
import com.music.recipes.itunes.domain.model.ITunesItemModel;
import com.music.recipes.itunes.presentation.BaseActivity;
import com.music.recipes.itunes.presentation.activitymain.adapter.MusicAdapter;
import com.music.recipes.itunes.presentation.di.activity.HasActivitySubcomponentBuilders;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainActivityView {

    @BindView(R.id.search_field)
    EditText songField;
    @BindView(R.id.music_recycler)
    RecyclerView musicRecycler;

    MusicAdapter adapter;

    @InjectPresenter
    MainActivityPresenter presenter;

    @ProvidePresenter
    MainActivityPresenter provideMainPresenter() {
        return (MainActivityPresenter) providePresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        adapter = new MusicAdapter(this);
        musicRecycler.setLayoutManager(new LinearLayoutManager(this));
        musicRecycler.setAdapter(adapter);

        if (savedInstanceState == null) {
            presenter.initMusicSearch(RxTextView.textChanges(songField));
        }
    }

    @Override
    protected void injectMembers(HasActivitySubcomponentBuilders hasActivitySubcomponentBuilders) {
        MainActivityComponent activityComponent = ((MainActivityComponent.Builder) hasActivitySubcomponentBuilders
                .getActivityComponentBuilder(MainActivity.class))
                .activityModule(new MainActivityComponent.MainActivityModule(this))
                .build();
        activityComponent.injectMembers(this);
        setActivityComponent(activityComponent);
    }

    @Override
    public void onMusicQueried(List<ITunesItemModel> answerModels) {
        adapter.setData(answerModels);
    }

    @Override
    public void errorOccurred(Throwable throwable) {
        Toast.makeText(this, "throwable:" + throwable, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }
}
