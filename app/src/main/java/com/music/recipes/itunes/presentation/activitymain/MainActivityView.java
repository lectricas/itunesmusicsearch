package com.music.recipes.itunes.presentation.activitymain;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.music.recipes.itunes.domain.model.ITunesItemModel;


import java.util.List;

/**
 * Created by lectricas on 30.06.2017.
 */

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainActivityView extends MvpView {
    void onMusicQueried(List<ITunesItemModel> answerModel);
    void errorOccurred(Throwable throwable);
}
