package com.music.recipes.itunes.presentation.activitymain.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frogermcs.recipes.itunes.R;
import com.music.recipes.itunes.domain.model.ITunesItemModel;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by lectricas on 02-Jul-17.
 */

public class MusicAdapter extends RecyclerView.Adapter<MusicItemViewHolder> {

    private final Context context;
    private final LayoutInflater inflater;

    private List<ITunesItemModel> data;

    public MusicAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MusicItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = inflater.inflate(R.layout.music_item, parent, false);
        return new MusicItemViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(MusicItemViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<ITunesItemModel> answerModels) {
        data.clear();
        data.addAll(answerModels);
        notifyDataSetChanged();
    }
}
