package com.music.recipes.itunes;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.arellomobile.mvp.MvpPresenter;
import com.music.recipes.itunes.domain.interact.MusicSearchInteract;
import com.music.recipes.itunes.presentation.activitymain.MainActivity;
import com.music.recipes.itunes.presentation.activitymain.MainActivityComponent;
import com.music.recipes.itunes.presentation.di.activity.PresenterProvider;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


@RunWith(AndroidJUnit4.class)
public class MainActivityUITest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class, true, false);

    @Mock
    MainActivityComponent.Builder builder;

    @Mock
    MusicSearchInteract interact;

    @Mock
    PresenterProvider presenterProvider;

    private MainActivityComponent mainActivityComponent = new MainActivityComponent() {
        @Override
        public void inject(PresenterProvider<MvpPresenter> presenterProvider) {
        }

        @Override
        public void injectMembers(MainActivity instance) {
            instance.setActivityComponent(this);
        }
    };

    @Before
    public void setUp() {
        when(builder.build()).thenReturn(mainActivityComponent);
        when(builder.activityModule(any(MainActivityComponent.MainActivityModule.class))).thenReturn(builder);
        mainActivityComponent.inject(presenterProvider);

        ApplicationMock app = (ApplicationMock) InstrumentationRegistry.getTargetContext().getApplicationContext();
        app.putActivityComponentBuilder(builder, MainActivity.class);
    }

    @Test
    public void checkTextView() {
//        activityRule.launchActivity(new Intent());
    }

}