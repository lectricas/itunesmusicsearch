package com.music.recipes.itunes.presentation.di.activity;

import android.app.Activity;

import com.arellomobile.mvp.MvpPresenter;
import dagger.MembersInjector;

public interface ActivityComponent<A extends Activity> extends MembersInjector<A> {
    void inject(PresenterProvider<MvpPresenter> presenterProvider);
}
