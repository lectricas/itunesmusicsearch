package com.music.recipes.itunes.presentation.activitymain;

import com.arellomobile.mvp.MvpPresenter;
import com.music.recipes.itunes.presentation.di.activity.ActivityComponent;
import com.music.recipes.itunes.presentation.di.activity.ActivityComponentBuilder;
import com.music.recipes.itunes.presentation.di.activity.ActivityModule;
import com.music.recipes.itunes.presentation.di.activity.ActivityScope;


import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;



@ActivityScope
@Subcomponent(
        modules = MainActivityComponent.MainActivityModule.class
)
public interface MainActivityComponent extends ActivityComponent<MainActivity> {

    @Subcomponent.Builder
    interface Builder extends ActivityComponentBuilder<MainActivityModule, MainActivityComponent> {
    }

    @Module(includes = MainActivityModule.Bindings.class)
    class MainActivityModule extends ActivityModule<MainActivity> {
        MainActivityModule(MainActivity activity) {
            super(activity);
        }

        @Module
        public interface Bindings {
            @Binds
            @ActivityScope
            MvpPresenter bindPresenter(MainActivityPresenter presenter);
        }
    }
}
